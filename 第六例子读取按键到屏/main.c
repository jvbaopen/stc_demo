/********************************************************************
日    期：2022/05/28
功    能: 读取点亮由FD650Bic驱动的电子屏的按键码显示到屏上，这里用到了模块化开发，以后不会这种做法    芯片型号STC15F104W	  
编    写：劲松电脑通讯	 www.jvbaopeng.com
*/

#include <fd650.h>
unsigned char G_num = 0;   //全局数字
unsigned char readint = 0;


void read650KEY(void){
	MS_U8 num ;
	num = MDrv_FD650_Read();
	if(num!=0){
    	G_num = num;
		readint = 0;
	}
}


void ET_0() interrupt 1 using 1                //定时器0中断
{	
	readint++;     //没事瞎玩，让扫描按键和屏显示省资源  
}

void EX_0() interrupt 0                //外部中断0,按键按了就中断下
{
	LED1 = ~LED1;	  //简单的灯开关
	G_num++;
        
}

void ET0_init()                //定时器0中断初始化
{
		TMOD=0x02;                //定时器0工作在方式2
        TH0=0x00;                //重载值
        TL0=0x00;                //初始值
        ET0=1;                //开启定时器0中断
        TR0=1;
}

void EX0_init()                //外部中断0初始化
{
        IT0=1;                //设置外部中断0为下降沿触发
        EX0=1;                //开启外部中断0
        EA=1;                //开启总中断
}


void main()
{
		int mm = 0;
		MDrv_FrontPnl_Update((MS_U8*)" OFF",0);	   //来个初始显示，可以直接显示字符串的，传入文本指针
        ET0_init();
		EX0_init();
        LED1 = 0;
        while(1)
        {	  
		   if(readint>50){	    //这样隔断时间扫描下按键是不是相对省资源点
		   		read650KEY();
           		MDrv_FrontPnl_Update(mystring(G_num), 0);
				readint = 0;
		   }  
        }
}