 /********************************************************************
日    期：2022/05/28
功    能: FD650芯片驱动屏驱动库   
编    写：劲松电脑通讯	 www.jvbaopeng.com
*/
#include	"fd650.h"
void  MDrv_FrontPnl_Update(MS_U8*,int);

//数码管显示常量
static const led_bitmap bcd_decode_tab[LEDMAPNUM] =
{	{'0', 0x3F}, {'1', 0x06}, {'2', 0x5B}, {'3', 0x4F},
    {'4', 0x66}, {'5', 0x6D}, {'6', 0x7D}, {'7', 0x07},
    {'8', 0x7F}, {'9', 0x6F}, {'a', 0x77}, {'A', 0x77},
    {'b', 0x7C}, {'B', 0x7C}, {'c', 0x58}, {'C', 0x39},
    {'d', 0x5E}, {'D', 0x5E}, {'e', 0x79}, {'E', 0x79},
    {'f', 0x71}, {'F', 0x71}, {'H', 0x76}, {'h', 0x74},
    {'o', 0x5C}, {'t', 0x78},
    {'l', 0x30}, {'L', 0x38}, {'N', 0x37},{'n', 0x37},
    {'p', 0x73},{'P', 0x73}, {'O', 0x3F}, {'u', 0x1C},
    {'U', 0x3E},{'S', 0x6D}, {'s', 0x6D},{'-', 0x40},
    {' ', 0x00}
};//BCD decode


//	微秒级延时
 void DELAY(MS_U8 i){
	while(i--);
}

 //显示屏开始信号：保持 CLK 为“1”电平，DAT 从“1”跳“0”，认为是开始信号
static  void MDrv_FD650_Start( void ){
	FD650_SDA = 1;
	FD650_SCL = 1;
	DELAY(5);
	FD650_SDA = 0;
	DELAY(5);
	FD650_SCL = 0;
}

//显示屏结束信号：保持 CLK 为“1”电平，DAT 从“0”跳“1”，认为是结束信号
static  void MDrv_FD650_Stop( void ){
	FD650_SDA = 0;
	FD650_SCL = 1;
	DELAY(5);
	FD650_SDA = 1;
}

//写一个字符
static void MDrv_FD650_WrByte( MS_U8 dat ){
    MS_U8 i; 
    for( i = 0; i != 8; i++ )   //数据循环8次，每一位判断数据是1还是0
    {
        if( dat & 0x80 ){	  //如dat为10010001，& 0x80，与10000000，结果为10000000，大于0，条件为真
            FD650_SDA = 1;     //所以只跟最前面的一位有关，首位为1结果为1，符合则让DTA输出1
        }else{
            FD650_SDA = 0;
        }
        DELAY(5);
        FD650_SCL = 1;     //一个时钟脉冲传输一位数据//CLK为高电平时数据必须保持不变
        dat <<= 1;
        DELAY(5);
        FD650_SCL = 0;      //直到CLK为低电平，一位数据传输结束
    }
    FD650_SDA = 1;     //写入，CLK从0跳到1，再从1跳到0	//写入650的内存
    DELAY(5);
    FD650_SCL = 1;
    DELAY(5);
    FD650_SCL = 0;
}

//写入数据命令
static void MDrv_FD650_Write( MS_U16 cmd ) //write cmd
{
    MDrv_FD650_Start();
    MDrv_FD650_WrByte(((MS_U8)(cmd>>7)&0x3E)|0x40);
    MDrv_FD650_WrByte((MS_U8)cmd);
    DELAY(2);
    MDrv_FD650_Stop();
    return;
}

/*读取要显示内容的对应地址 */
static MS_U8 MDrv_Led_Get_Code(MS_U8 cTemp){
    MS_U8 i, bitmap = 0x00;
    for(i = 0; i < LEDMAPNUM; i++){
        if(bcd_decode_tab[i].character == cTemp){
		     bitmap = bcd_decode_tab[i].bitmap;
             break;
         }
    }
    return bitmap;
}

/*
*更新数据(文本数据地址，是否带中间两点)
*点亮数码管
*/
void MDrv_FrontPnl_Update(MS_U8 *u8str, MS_BOOL Colon_flag){ 
    int i;
    MS_U8 LedBuffer[4];
    if(!u8str) return;
    for(i = 0; i < FP_LED_MAX_NUM; i++){	   // FP_LED_MAX_NUM 最大显示位数，在.h文件里定义了，这里是4
        LedBuffer[i] = MDrv_Led_Get_Code(u8str[i]);
    }
    MDrv_FD650_Write(FD650_SYSON_4);// 开启显示和键盘，4段显示方式
    //发显示数据
    MDrv_FD650_Write( FD650_DIG0 | LedBuffer[0] ); //点亮第一个数码管
    if(Colon_flag){
		MDrv_FD650_Write( FD650_DIG1 | LedBuffer[1] | FD650_DOT ); //点亮小数点
	}
	MDrv_FD650_Write( FD650_DIG1 | LedBuffer[1] );
	MDrv_FD650_Write( FD650_DIG2 | LedBuffer[2]); //点亮第三个数码管
	MDrv_FD650_Write( FD650_DIG3 | LedBuffer[3] | 0); //点亮第四个数码管		
}


//数字转文本(待转换数字)
MS_U8 *mystring(int Num){
	static  MS_U8 dat[4];
	dat[0]=Num%10000/1000+'0';
	dat[1]=Num%1000/100+'0';
	dat[2]=Num%100/10+'0';
	dat[3]=Num%10+'0';
	return dat;
}

//读出按键数据
static int MDrv_FD650_RdByte( void )
{
    int dat,i;
    FD650_SDA = 1;	      //该命令属于读操作，是唯一的具有数据返回的命令，单片机必须先释放DAT引脚	
    dat = 0;
	for( i = 0; i != 8; i++ ){
		FD650_SCL=1;				   //SCL数据从高变低时，SDA输出数据
		DELAY(5);
		dat <<= 1;
		if( FD650_SDA) dat++;
		FD650_SCL=0;
    }
    FD650_SDA = 1;
    DELAY(5);
    FD650_SCL = 1; 
    DELAY(5);
    FD650_SCL = 0; 
    return dat;
}

/****************************************************************
* Function Name:FD650_Read
* Description:读取按键按下状态的键值，如读到无效按键值返回0
* return：按键按下状态的键值
*/
int MDrv_FD650_Read( void )
{
    int keycode = 0;
    MDrv_FD650_Start();
    MDrv_FD650_WrByte(0x4F);	 //这里0X4F看手册得出
    keycode = MDrv_FD650_RdByte();
    DELAY(2);
    MDrv_FD650_Stop();
	if((keycode&0x00000040) ==0) keycode = 0;	  //加上这句才是读取的按键按下时候的状态值
    return keycode;
}


 /**  显示屏设置字符
static int set_fd650_led_value(char *display_code)
{
    int i,j = 0;
    char data,display_char[8];
    int dot = 0;
    for(i = 0; i <= 8; i++) {
        data = display_code;
        if(data == ':')    {
          dot++;
        }
        else{
            display_char[j++] = display_code;
            if(data == '\0')break;
        }
    }
    MDrv_FrontPnl_Update((MS_U8*)display_char, dot);
    return 0;
}


//显示屏初始化
void MDrv_FrontPnl_Init(void)
{
   	MDrv_FD650_Write(FD650_SYSON_4);
    MDrv_FrontPnl_Update((MS_U8*)"boot", 0);
}

//关闭显示屏
void MDrv_FrontPnl_Suspend(int num){
	if(num==1){
		MDrv_FD650_Write(FD650_SYSOFF);
	}else{
		 MDrv_FrontPnl_Update((MS_U8*)" OFF", 0);
	}
} 
 **/  