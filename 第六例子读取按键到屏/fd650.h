 /********************************************************************
日    期：2022/05/28
功    能: FD650芯片驱动屏驱动库   基本用法 显示 MDrv_FrontPnl_Update(mystring(待显示文本),0);    取按键 MDrv_FD650_Read()
编    写：劲松电脑通讯	 www.jvbaopeng.com
*/
#ifndef __FP__FD650__H
#define __FP__FD650__H

#include <config.h>
/*** type redefine ***/
typedef unsigned char MS_U8;
typedef unsigned short MS_U16;
typedef unsigned long MS_U32;
typedef int MS_S32;
typedef int MS_BOOL;


/**************************** FD650的一些常量设置***************************** */
#define LEDMAPNUM 40

//key mapping
typedef struct
{
  MS_U8 keyMapData;
  MS_U8 keyMapLevel;

} MS_KEYMAP;

//led mapping
typedef struct _led_bitmap
{
  MS_U8 character;
  MS_U8 bitmap;
} led_bitmap;



/* *************************************硬件相关*********************************************** */
#define HIGH 1
#define LOW  0

#define FRONTPNL_START_TIME_MS      3   //((1000 / 50) / LED_NUM)
#define FRONTPNL_PERIOD_TIME_MS     150
#define FP_LED_MAX_NUM                 4
#define FrontPnl_MSECS_TICK         100 //100 msecs/tick
#define FrontPnl_TICKS_SEC          1    //10 ticks/sec
/* ********************************************************************************************* */
// 设置系统参数命令

#define FD650_BIT_ENABLE  0x01    // 开启/关闭位
#define FD650_BIT_SLEEP   0x04    // 睡眠控制位
#define FD650_BIT_7SEG    0x08    // 7段控制位
#define FD650_BIT_INTENS1 0x10    // 1级亮度
#define FD650_BIT_INTENS2 0x20    // 2级亮度
#define FD650_BIT_INTENS3 0x30    // 3级亮度
#define FD650_BIT_INTENS4 0x40    // 4级亮度
#define FD650_BIT_INTENS5 0x50    // 5级亮度
#define FD650_BIT_INTENS6 0x60    // 6级亮度
#define FD650_BIT_INTENS7 0x70    // 7级亮度
#define FD650_BIT_INTENS8 0x00    // 8级亮度

#define FD650_SYSOFF  0x0400      // 关闭显示、关闭键盘
#define FD650_SYSON ( FD650_SYSOFF | FD650_BIT_ENABLE ) // 开启显示、键盘
#define FD650_SLEEPOFF  FD650_SYSOFF  // 关闭睡眠
#define FD650_SLEEPON ( FD650_SYSOFF | FD650_BIT_SLEEP )  // 开启睡眠
#define FD650_7SEG_ON ( FD650_SYSON | FD650_BIT_7SEG )  // 开启七段模式
#define FD650_8SEG_ON ( FD650_SYSON | 0x00 )  // 开启八段模式
#define FD650_SYSON_1 ( FD650_SYSON | FD650_BIT_INTENS1 ) // 开启显示、键盘、1级亮度
#define FD650_SYSON_4 ( FD650_SYSON | FD650_BIT_INTENS4 ) // 开启显示、键盘、4级亮度
#define FD650_SYSON_8 ( FD650_SYSON | FD650_BIT_INTENS8 ) // 开启显示、键盘、8级亮度
// 加载字数据命令
#define FD650_DIG0    0x1400      // 数码管位0显示,需另加8位数据
#define FD650_DIG1    0x1500      // 数码管位1显示,需另加8位数据
#define FD650_DIG2    0x1600      // 数码管位2显示,需另加8位数据
#define FD650_DIG3    0x1700      // 数码管位3显示,需另加8位数据
#define FD650_DOT     0x0080      // 数码管小数点显示   


void DELAY(MS_U8 i);
void MDrv_FD650_Start(void);
void MDrv_FD650_Stop( void );
void MDrv_FD650_WrByte( MS_U8 dat );
MS_U8 MDrv_Led_Get_Code(MS_U8 cTemp);

void  MDrv_FrontPnl_Update(MS_U8*,int);    //写入数据
int  MDrv_FD650_Read( void );   //读出键盘数据 
 MS_U8 * mystring(int);    //文本转换成字符，如1转为0001


#endif