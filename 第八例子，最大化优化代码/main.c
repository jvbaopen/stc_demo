/********************************************************************
日    期：2022/06/1
功    能: 第六节代码最大化的优化，以前占3k，看最后能不能优化到2k以内    芯片型号STC15F104W	  
编    写：劲松电脑通讯	 www.jvbaopeng.com
*/

#include <config.h>

#include <fd650.h>
#include <irnec.h>



//========================================================================
// 函数: void  delay_ms(unsigned char ms)
// 描述: 延时函数。
// 参数: ms,要延时的ms数, 这里只支持1~255ms. 自动适应主时钟.
// 返回: none.
// 版本: VER1.0
// 日期: 2013-4-1
// 备注: 
//========================================================================
void  delay_ms(unsigned char ms)
{
     unsigned int i;
	 do{
	      i = MAIN_Fosc / 13000;
		  while(--i)	;   //14T per loop
     }while(--ms);
}








void main()
{
		MDrv_FrontPnl_Update((MS_U8*)" OFF",0);	   //来个初始显示，可以直接显示字符串的，传入文本指针
        ET0_init();
		EX0_init();
        LED1 = 0;
        while(1)
        {	
		   if(DATA_end==1){
		   		LED1 = 1;	   //指示灯也来个同步
		   		decode();		 //解码
				DATA_end = 0;
		   }
		   if(DECODE_end==1){  		//解码完成
		   		DECODE_end = 0; 
           		MDrv_FrontPnl_Update(mystring(Code0[0]), 0);
				delay_ms(150);
				delay_ms(150);
				MDrv_FrontPnl_Update(mystring(Code0[2]), 0);
				LED1 = 0;
				
		   }  
        }
}