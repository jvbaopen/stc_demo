/********************************************************************
日    期：2022/05/28
功    能: 读取点亮由FD650Bic驱动的电子屏的按键码显示到屏上，这里用到了模块化开发    芯片型号STC15F104W	  
编    写：劲松电脑通讯	 www.jvbaopeng.com
*/

#include <config.h>
#include <fd650.h>		//显示
#include <irnec.h>		//解码
#include  "delay.h"	    //延迟










void main()
{
		LED1 = 0;
		MDrv_FrontPnl_Update((MS_U8*)" OFF",0);	   //来个初始显示，可以直接显示字符串的，传入文本指针
		IR_Init();    //定时器初始化 
        while(1){	  
    	if (IR_BT > 0){                                                         //正确解码
				LED1 = 1;
			  	MDrv_FrontPnl_Update(mystring(NEC[0]), 0);   //用户吗高8位
				delay_ms(250);	  //因为参数最大不能大于256，这里延迟两下
				delay_ms(250);
				//MDrv_FrontPnl_Update(mystring(NEC[1]), 0);    //用户吗低8位
				//delay_ms(300);
				MDrv_FrontPnl_Update(mystring(NEC[2]), 0);    //键值
				LED1 = 0;
				IR_BT = 0;
			}
        } 
}