//*********************【 NEC解码头文件 】*******************
//
//     简介：本程序适用于NCE解码：（9ms+4.5ms）引导码+32位编码。
//           兼容STC所有型号（包括 1T 和 12T 系列），可以定义任意I/O作红外接收脚，
//                         自适应解码主频：6MHz ~ 40MHz。
//
// 使用条件：占用系统定时器0，开启定时器0中断（如使用其它定时器请自改IR_Init();初始化函数）
//
// 使用说明：填相关宏定义：USER_H、USER_L、Check_EN、CPU_Fosc、IR，
//           上电初始化函数IR_Init()，
//           在定时器0中断中调用IR_NEC()解码函数，
//           解码有效时，IR_BT=2即短按，IR_BT=3即长按，由用户清0，
//           解码存放：用户码高8位NEC[0]，用户码低8位NEC[1]，操作码NEC[2]，操作码反码NEC[3]。
//
//【供用户调用的函数】
//    IR_Init();                         //接收初始化，开启定时器0中断400us
//    IR_NEC();                             //红外线解码（解NEC编码）
//         
//***************************************************************/
#include   "config.h"	    //这个扩展文件好，基本东西都在这里
#include   "irnec.h"


unsigned char IR_BT;     //解码效果返回：0无效，1有效，2短按，3长按
unsigned char  NEC[4];    //解码存放：16位用户码、操作码正反码
unsigned char cntCA;     //长按计数
unsigned int  cntStep;   //步数计
bit                          IRa,IRb;   //接收脚电位状态保存
bit                          IRsync;    //同步标志
unsigned char BitN;      //位码装载数





void IR_NEC()
{         
   TL0 = TH_L;   //重赋值
   TH0 = TH_H;      

   cntStep++;    //步数累加
   if(IR_BT==1)if(cntStep>300)IR_BT=2; //解码有效后，如果无长按，120ms（400us×300）后默认短按

   IRb = IRa;    //保存上次电位状态
   IRa = IR;     //保存当前电位状态
      
   if(IRb && !IRa)    //是否下降沿（上次高，当前低）
   {
      if(cntStep > Boot_Limit)   //超过同步时间？
      {      
          if(IR_BT==1)if(++cntCA>CA_S)IR_BT=3; //解码有效后，继续按住遥控>CA_S即长按
          IRsync=0;                            //同步位清0
      }
      else if(cntStep > Boot_Lower){ IRsync=1; BitN=32; }   //同步位置1，装载位码数32                          
      else if(IRsync)            //如果已同步
      {
         if(cntStep > Bit1_Limit)IRsync=0;                  
         else
         {      
            NEC[3] >>= 1;                              
            if(cntStep > Bit0_Limit)NEC[3] |= 0x80;    //“0”与“1”
            if(--BitN == 0)                              
            {
               IRsync = 0;    //同步位清0

               #if (Check_EN == 1)                                       
               if((NEC[0]==USER_H)&&(NEC[1]==USER_L)&&(NEC[2]==~NEC[3]))    //校验16位用户码、操作码正反码
               {  IR_BT=1; cntCA=0;  }     //解码有效，接下来判断：短按？长按？
               #else
               if(NEC[2]==~NEC[3]){ IR_BT=1; cntCA=0; }  //只校验操作码正反码
               #endif                                       
            }
            else if((BitN & 0x07)== 0)    //NEC[3]每装满8位，移动保存一次（即 BitN%8 == 0）
            {   NEC[0]=NEC[1]; NEC[1]=NEC[2]; NEC[2]=NEC[3];   }
         }
      }
      cntStep = 0;   //步数计清0
   }
}


void ET_0() interrupt 1 using 1                //定时器0中断
{		 
	IR_NEC();
}

 void IR_Init(void){
	   AUXR |= 0x80;           //定时器设置为1t模式
	   TMOD |= 0x01;           //定时器设置为16位自动加载
	   TL0 = TH_L;             //高8位计时  
	   TH0 = TH_H;			   //低8位计时
	   ET0 = 1;				   //TO的允许中断位 1允许 0禁止
	   EA  = 1;			       //开启所有中断
	   TR0 = 1;	 
}