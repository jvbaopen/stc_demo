/********************************************************************
日    期：2022/05/30
功    能: 遥控解码支持库
编    写：劲松电脑通讯	 www.jvbaopeng.com
*/
/*
NEC协议简要记录
NEC协议是红外遥控协议的一种，由其编码的数据帧分别由引导码、用户码、用户码（或者是用户码的反码）、数据码（即按键码）和数据码的反码这五部分组成，
最后还有一个停止位。引导码表示即将开始传输32位的二进制数据；引导码之后的部分长度为4字节一共32位；
第一字节为用户码；第二字节可能是用户码，也可能是用户码的反码，具体由厂商决定；第三字节是当前按键的按键码；
第四字节是按键码的反码；停止位主要起隔离作用，一般不进行判断，也不需要理会。每一字节的数据从低位到高位依次发送。
NEC协议编码方式
NEC协议的每一比特数据本身也需要进行编码，编码后再进行载波调制。
引导码：9ms的高电平 + 4.5ms的低电平
比特值0：0.56ms的高电平 + 0.56ms的低电平
比特值1：0.56ms的高电平 + 1.68ms的低电平
发射端和接收端的电平相反，即：

引导码：9ms的低电平 + 4.5ms的高电平
比特值0：0.56ms的低电平 + 0.56ms的高电平
比特值1：0.56ms的低电平 + 1.68ms的高电平
*/        
//***************************************************************/
#ifndef		__IRNEC_H
#define		__IRNEC_H

#define  USER_H     0x01         //用户码高8位
#define  USER_L     0xFB         //用户码低8位
#define  Check_EN   0            //是否要校验16位用户码：不校验填0，校验则填1      
#define  CPU_Fosc   12000000L    //输入主频，自适应解码（单位：Hz，范围：6MHz ~ 40MHz）
#define  CA_S       8            //长按时间设置，单位：108mS（即 108mS整数倍，10倍以上为宜）
#define  Step       400          //红外采样步长：400us
#define  TH_H      ((65536-Step*(CPU_Fosc/1000)/1000)>>8)  //定时器高8位基准赋值
#define  TH_L      ((65536-Step*(CPU_Fosc/1000)/1000)&0x00FF)  //定时器低8位基准赋值
/*┈┈┈┈┈┈┈┈┈┈ 基准 ┈┈┈┈┈┈┈┈┈┈┈*/
#define    Boot_Limit    ((9000+4500 +1000)/Step)   //引导码周期上限   
#define    Boot_Lower    ((9000+4500 -1000)/Step)   //引导码周期下限   
#define    Bit1_Limit    ((2250 +800)/Step)         //“1”周期上限
#define    Bit0_Limit    ((1125 +400)/Step)         //“0”周期上限
/*┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈*/
extern unsigned char IR_BT;     //解码效果返回：0无效，1有效，2短按，3长按
extern unsigned char  NEC[4];    //解码存放：16位用户码、操作码正反码

extern void IR_NEC();
extern void IR_Init(void);

#endif